export interface Note {
    "idNote": string,
    "value": string,
    "complete": boolean | string
  }
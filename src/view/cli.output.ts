import { Note } from '../types/todo.interface.js';
import {usageText} from '../controller/schema.js'
import log from '@ajar/marker';

export function show(arrNots: Note[]) {
    for (const note of arrNots) {
      if (note.complete) note.complete = "\u2705";
      else note.complete = "\u274E"
    }
    console.table(arrNots);
  }

// used to log errors to the console in red color
export function errorLog(error: string) {
  log.red(error)
}

export const usage = function () {
  log.magenta(usageText)
}
